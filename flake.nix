{
  description = "A basic flake with a shell";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix-src.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix-src, ... }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ poetry2nix-src.overlay ];
      };

      env = pkgs.poetry2nix.mkPoetryEnv {
        projectDir = ./.;
        python = pkgs.python39;
        #preferWheels = true;
        overrides = pkgs.poetry2nix.overrides.withDefaults(self: super: {
          typing-extensions = super.typing-extensions.overridePythonAttrs (old: {
            propagatedBuildInputs = (old.propagatedBuildInputs or [ ]) ++ [
              self.flit-core
            ];
          });
        });
      };
    in {
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
          poetry
          env
          graphviz
        ];
      };
    });
}
