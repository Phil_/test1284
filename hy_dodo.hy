(import pathlib)
(import pygraphviz)
(import functools)


(defn module_to_dot [dependencies targets]
  (setv graph (pygraphviz.AGraph :strict False :directed True))
  (for [dep dependencies]
    (setv filepath (pathlib.Path dep))
    (with [fh (filepath.open)]
      (for [line fh]
        (if line
          (graph.add_edge filepath.stem (.strip line))))))
  (graph.write (get targets 0)))


(defn task_imports [] {
    "file_dep" ["projects/requests/requests/models.py"]
    "targets" ["requests.models.deps"]
    "actions" ["python -m import_deps %(dependencies)s > %(targets)s"]})


(defn task_dot [] {
    "file_dep" ["requests.models.deps"]
    "targets" ["requests.models.dot"]
    "actions" [module_to_dot]})


(defn task_draw [] {
    "file_dep" ["requests.models.dot"]
    "targets" ["requests.models.png"]
    "actions" ["dot -Tpng %(dependencies)s -o %(targets)s"]})
